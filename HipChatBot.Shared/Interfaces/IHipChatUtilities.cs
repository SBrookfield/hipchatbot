﻿#region Copyright

// Filename: IHipChatUtilities.cs
// Date Created: 19/05/2016 21:03
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using HipChatBot.Shared.Models;

namespace HipChatBot.Shared.Interfaces
{
    public interface IHipChatUtilities
    {
        User GetUser(int userId);
        void SendNotification(int roomId, string message, bool notify = false);
    }
}