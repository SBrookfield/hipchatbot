﻿#region Copyright

// Filename: Notification.cs
// Date Created: 18/05/2016 14:02
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

namespace HipChatBot.Shared.Models
{
    public class Notification
    {
        public string MessageFormat { get; set; }
        public string Color { get; set; }
        public string Message { get; set; }
        public string Notify { get; set; }

        public Notification(string message, string messageFormat = "text", string notify = "false", string color = "purple")
        {
            MessageFormat = messageFormat;
            Color = color;
            Message = message;
            Notify = notify;
        }
    }
}