﻿#region Copyright

// Filename: RoomTopicChangedMessage.cs
// Date Created: 18/05/2016 23:12
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

namespace HipChatBot.Shared.Messages
{
    public class RoomTopicChangedMessage
    {
        public int RoomId { get; set; }
        public int ChangerId { get; set; }
        public string Topic { get; set; }

        public RoomTopicChangedMessage(int roomId, int changerId, string topic)
        {
            RoomId = roomId;
            ChangerId = changerId;
            Topic = topic;
        }
    }
}