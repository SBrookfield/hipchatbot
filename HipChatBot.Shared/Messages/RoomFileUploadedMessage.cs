﻿#region Copyright

// Filename: RoomFileUploadedMessage.cs
// Date Created: 18/05/2016 22:53
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

namespace HipChatBot.Shared.Messages
{
    public class RoomFileUploadedMessage
    {
        public int RoomId { get; private set; }
        public int SenderId { get; private set; }
        public string FileName { get; private set; }
        public long FileSize { get; private set; }
        public string FileUrl { get; private set; }

        public RoomFileUploadedMessage(int roomId, int senderId, string fileName, string fileUrl, long fileSize)
        {
            RoomId = roomId;
            SenderId = senderId;
            FileName = fileName;
            FileUrl = fileUrl;
            FileSize = fileSize;
        }
    }
}