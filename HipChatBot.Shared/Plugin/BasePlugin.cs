﻿#region Copyright

// Filename: BasePlugin.cs
// Date Created: 19/05/2016 20:40
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using HipChatBot.Shared.Interfaces;

namespace HipChatBot.Shared.Plugin
{
    public abstract class BasePlugin
    {
        protected IMessageBus MessageBus { get; }
        protected IHipChatUtilities HipChatUtilities { get; }
        protected ITracing Tracing { get; }

        protected BasePlugin(IMessageBus messageBus, IHipChatUtilities hipChatUtilities, ITracing tracing)
        {
            MessageBus = messageBus;
            HipChatUtilities = hipChatUtilities;
            Tracing = tracing;
        }

        public abstract void Start();

        public abstract void Stop();

        private BasePlugin()
        {
        }
    }
}