﻿#region Copyright

// Filename: HelloWorldPlugin.cs
// Date Created: 19/05/2016 22:05
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System.Configuration;
using System.Diagnostics;
using System.Text.RegularExpressions;
using HipChatBot.Shared.Interfaces;
using HipChatBot.Shared.Messages;
using HipChatBot.Shared.Plugin;

namespace HipChatBot.HelloWorldPlugin
{
    public class HelloWorldPlugin : BasePlugin
    {
        public HelloWorldPlugin(IMessageBus messageBus, IHipChatUtilities hipChatUtilities, ITracing tracing) : base(messageBus, hipChatUtilities, tracing)
        {
        }

        public override void Start()
        {
            SubscribeToMessages();
        }

        public override void Stop()
        {
            HipChatUtilities.SendNotification(1, "I'm going now, goodbye!");
        }

        private void SubscribeToMessages()
        {
            MessageBus.Subscribe<RoomMessagedMessage>(RoomMessageReceived);
        }

        private void RoomMessageReceived(RoomMessagedMessage message)
        {
            using (Tracing.NewScope())
            {
                if (!Regex.IsMatch(message.Message, $"@{ConfigurationManager.AppSettings["integrationName"]}", RegexOptions.IgnoreCase))
                    return;

                if (!Regex.IsMatch(message.Message, "\\s?Hello.*", RegexOptions.IgnoreCase))
                    return;

                Tracing.Trace(TraceEventType.Verbose, "Replying to Hello message...");
                Tracing.Trace(TraceEventType.Verbose, "Getting user's name...");
                var userName = HipChatUtilities.GetUser(message.SenderId).Name;
                Tracing.Trace(TraceEventType.Verbose, "Sending reply...");
                HipChatUtilities.SendNotification(message.RoomId, $"Hello {userName}!", true);
            }
        }
    }
}