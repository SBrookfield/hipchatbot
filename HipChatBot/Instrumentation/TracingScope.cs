﻿#region Copyright

// Filename: TracingScope.cs
// Date Created: 09/05/2016 13:34
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Diagnostics;
using HipChatBot.Shared.Interfaces;

namespace HipChatBot.Instrumentation
{
    public class TracingScope : ITracingScope
    {
        private readonly TraceSource _traceSource;
        private readonly Guid _oldActivityId;
        private readonly string _activityName;

        public TracingScope(TraceSource traceSource, string activityName)
        {
            if ((_traceSource = traceSource) == null)
                return;

            _oldActivityId = Trace.CorrelationManager.ActivityId;

            var newActivityId = Guid.NewGuid();

            if (_oldActivityId != Guid.Empty)
            {
                _traceSource.TraceTransfer(0, "Transferring to new activity...", newActivityId);

                if (Tracing.TraceToConsole)
                    Console.WriteLine($"-- Starting activity \"{activityName}\" ---");
            }

            Trace.CorrelationManager.ActivityId = newActivityId;
            _traceSource.TraceEvent(TraceEventType.Start, 0, _activityName = activityName);
        }

        public void Dispose()
        {
            if (_traceSource == null)
                return;

            if (_oldActivityId != Guid.Empty)
            {
                _traceSource.TraceTransfer(0, "Transferring back to previous activity...", _oldActivityId);

                if (Tracing.TraceToConsole)
                    Console.WriteLine($"-- Stopping activity \"{_activityName}\" ---");
            }

            _traceSource.TraceEvent(TraceEventType.Stop, 0, _activityName);
            Trace.CorrelationManager.ActivityId = _oldActivityId;
        }
    }
}