﻿#region Copyright

// Filename: Tracing.cs
// Date Created: 09/05/2016 13:07
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Configuration;
using System.Diagnostics;
using HipChatBot.Shared.Interfaces;

namespace HipChatBot.Instrumentation
{
    public class Tracing : ITracing
    {
        public static bool TraceToConsole { get; }
        public static Tracing Current => _current ?? (_current = new Tracing());

        private static Tracing _current;
        private static readonly TraceSource _traceSource;

        static Tracing()
        {
            TraceToConsole = bool.Parse(ConfigurationManager.AppSettings["traceToConsole"]);
            _traceSource = new TraceSource(nameof(HipChatBot));
        }

        public ITracingScope NewScope()
        {
            var callingMethod = new StackTrace().GetFrame(1).GetMethod();
            var callingClassName = callingMethod.ReflectedType?.FullName ?? "Unknown";
            var fullCallerMethodName = $"{callingClassName}.{callingMethod.Name}";
            return new TracingScope(_traceSource, fullCallerMethodName);
        }

        public void Trace(TraceEventType type, string message)
        {
            try
            {
                _traceSource?.TraceEvent(type, 0, message);
                
                if (TraceToConsole)
                    Console.WriteLine(message);
            }
            catch (Exception)
            {
                // Suppressing trace exceptions.
            }
        }
    }
}