﻿#region Copyright

// Filename: DateStampedStream.cs
// Date Created: 09/05/2016 13:18
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.IO;

namespace HipChatBot.Instrumentation
{
    public class DateStampedStream : FileStream
    {
        public DateStampedStream(string filePath) : base(DateStampedFileName(filePath), FileMode.OpenOrCreate)
        {
        }

        private static string DateStampedFileName(string filePath)
        {
            var extension = Path.GetExtension(filePath);
            var directoryName = Path.GetDirectoryName(filePath);

            if (directoryName == "")
                directoryName = AppDomain.CurrentDomain.BaseDirectory;

            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);

            if (extension == null || directoryName == null)
                return "Default.svclog";

            return Path.Combine(directoryName, $"{fileNameWithoutExtension}_{DateTime.Now.ToString("yyyy-MM-dd")}{extension}");
        }
    }
}