﻿#region Copyright

// Filename: HipChatUtilities.cs
// Date Created: 18/05/2016 18:32
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using HipChatBot.ExtensionMethods;
using HipChatBot.Instrumentation;
using HipChatBot.Managers;
using HipChatBot.Models;
using HipChatBot.Shared.Interfaces;
using HipChatBot.Shared.Models;
using Newtonsoft.Json;

namespace HipChatBot
{
    public class HipChatUtilities : IHipChatUtilities
    {
        public void SendNotification(int roomId, string message, bool notify = false)
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Verbose, $"Sending notification to room with id {roomId}.");
                SendJsonObject($"/v2/room/{roomId}/notification", new Notification(message, notify: notify.ToString().ToLower()));
            }
        }

        public User GetUser(int userId)
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Verbose, $"Getting user information for user Id {userId}.");
                var userModel = GetObject<UserModel>($"/v2/user/{userId}");
                return new User(userModel.Id, userModel.Title, userModel.Name, userModel.MentionName);
            }
        }

        private void SendJsonObject(string partUri, object obj, bool includeAuthToken = true)
        {
            using (var webClient = new WebClient())
            {
                if (includeAuthToken)
                    webClient.Headers.Add(HttpRequestHeader.Authorization, $"Bearer {OAuthManager.AccessToken}");

                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                var hipChatUri = new Uri(ConfigurationManager.AppSettings["hipChatApiAddress"]);
                var fullUri = new Uri(hipChatUri, partUri);

                Tracing.Current.Trace(TraceEventType.Verbose, $"Sending JSON to \"{fullUri.AbsoluteUri}\".");
                webClient.UploadString(fullUri, obj.ToJson());
            }
        }

        private T GetObject<T>(string partUri, bool includeAuthToken = true) where T : class
        {
            using (var webClient = new WebClient())
            {
                if (includeAuthToken)
                    webClient.Headers.Add(HttpRequestHeader.Authorization, $"Bearer {OAuthManager.AccessToken}");

                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                var hipChatUri = new Uri(ConfigurationManager.AppSettings["hipChatApiAddress"]);
                var fullUri = new Uri(hipChatUri, partUri);

                Tracing.Current.Trace(TraceEventType.Verbose, $"Getting JSON from \"{fullUri.AbsoluteUri}\".");
                return JsonConvert.DeserializeObject<T>(webClient.DownloadString(fullUri));
            }
        }
    }
}