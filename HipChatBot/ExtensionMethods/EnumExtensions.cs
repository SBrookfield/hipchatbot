﻿#region Copyright

// Filename: EnumExtensions.cs
// Date Created: 17/05/2016 21:16
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.ComponentModel;
using System.Linq;

namespace HipChatBot.ExtensionMethods
{
    public static class EnumExtensions
    {
        public static string Description(this Enum enumValue)
        {
            var descriptionAttribute = enumValue
                .GetType()
                .GetField(enumValue.ToString())
                .GetCustomAttributes(typeof (DescriptionAttribute), false)
                .Cast<DescriptionAttribute>()
                .FirstOrDefault();

            return descriptionAttribute != null 
                ? descriptionAttribute.Description 
                : enumValue.ToString();
        }
    }
}