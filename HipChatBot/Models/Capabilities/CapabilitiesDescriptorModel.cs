#region Copyright

// Filename: CapabilitiesDescriptorModel.cs
// Date Created: 19/05/2016 21:52
// Copyright � 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

namespace HipChatBot.Models.Capabilities
{
    public class CapabilitiesDescriptorModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Key { get; set; }
        public VendorModel Vendor { get; set; }
        public LinkModel Links { get; set; }
        public CapabilitiesModel Capabilities { get; set; }
    }
}