#region Copyright

// Filename: FileModel.cs
// Date Created: 19/05/2016 21:51
// Copyright � 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using Newtonsoft.Json;

namespace HipChatBot.Models.Webhook
{
    public class FileModel
    {
        public string Name { get; set; }
        public long Size { get; set; }
        [JsonProperty("thumb_url")]
        public string ThumbnailUrl { get; set; }
        public string Url { get; set; }
    }
}