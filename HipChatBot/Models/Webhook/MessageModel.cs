#region Copyright

// Filename: MessageModel.cs
// Date Created: 19/05/2016 21:51
// Copyright � 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using Newtonsoft.Json;

namespace HipChatBot.Models.Webhook
{
    public class MessageModel
    {
        [JsonProperty("date")]
        public DateTime DateSent { get; set; }
        [JsonProperty("from")]
        public UserModel User { get; set; }
        public UserModel[] Mentions { get; set; }
        [JsonProperty("message")]
        public string Text { get; set; }
    }
}