﻿#region Copyright

// Filename: HipChatBot.cs
// Date Created: 07/05/2016 00:35
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using HipChatBot.Instrumentation;
using HipChatBot.Shared.Plugin;

namespace HipChatBot
{
    public class Plugins
    {
        private static Dictionary<object, MethodInfo> _stopMethodInfoByInstance;

        public Plugins()
        {
            _stopMethodInfoByInstance = new Dictionary<object, MethodInfo>();
        }

        public void Start()
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Information, "Loading all plugins...");
                try
                {
                    var currentDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    var pluginFolder = Path.Combine(currentDirectory, "Plugins");

                    if (!Directory.Exists(pluginFolder))
                    {
                        Tracing.Current.Trace(TraceEventType.Information, "Plugins folder does not exist, creating folder.");
                        Directory.CreateDirectory(pluginFolder);
                        return;
                    }

                    var pluginBaseType = typeof (BasePlugin);
                    var constructorArgs = new object[] {MessageBus.Current, new HipChatUtilities(), Tracing.Current};

                    foreach (var assemblyFilePath in Directory.GetFiles(pluginFolder, "*.dll", SearchOption.AllDirectories))
                    {
                        Tracing.Current.Trace(TraceEventType.Verbose, $"Loading assembly \"{assemblyFilePath}\"...");
                        var assembly = Assembly.LoadFile(assemblyFilePath);
                        Tracing.Current.Trace(TraceEventType.Verbose, "Loading plugin classes...");
                        var pluginTypes = assembly.GetTypes().Where(t => t != pluginBaseType && pluginBaseType.IsAssignableFrom(t));

                        foreach (var type in pluginTypes)
                        {
                            try
                            {
                                Tracing.Current.Trace(TraceEventType.Verbose, $"Getting required methods for \"{type.Name}\"...");
                                var startMethod = type.GetMethod(nameof(BasePlugin.Start));
                                var stopMethod = type.GetMethod(nameof(BasePlugin.Stop));

                                Tracing.Current.Trace(TraceEventType.Verbose, $"Creating instance of \"{type.Name}\"...");
                                var instance = Activator.CreateInstance(type, constructorArgs);

                                _stopMethodInfoByInstance.Add(instance, stopMethod);

                                Tracing.Current.Trace(TraceEventType.Information, $"Starting plugin \"{type.Name}\"...");
                                startMethod.Invoke(instance, null);
                            }
                            catch (Exception exception)
                            {
                                Tracing.Current.Trace(TraceEventType.Error, $"Failed to load plugin for \"{type.Name}\". {exception.Message}");
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    Tracing.Current.Trace(TraceEventType.Error, $"Failed to load plugins. {exception.Message}");
                }
            }
        }

        public void Stop()
        {
            Tracing.Current.Trace(TraceEventType.Information, "Stopping all loaded plugins...");

            foreach (var key in _stopMethodInfoByInstance.Keys)
            {
                try
                {
                    _stopMethodInfoByInstance[key].Invoke(key, null);
                }
                catch
                {
                    // Ignore errors since we're stopping.
                }
            }
        }
    }
}