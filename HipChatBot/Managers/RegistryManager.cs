﻿#region Copyright

// Filename: RegistryManager.cs
// Date Created: 10/05/2016 20:32
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Diagnostics;
using HipChatBot.Instrumentation;
using Microsoft.Win32;

namespace HipChatBot.Managers
{
    public static class RegistryManager
    {
        public static void Save<T>(T obj) where T : class
        {
            using (Tracing.Current.NewScope())
            {
                var className = typeof(T).Name;
                Tracing.Current.Trace(TraceEventType.Verbose, $"Saving registry settings for \"{className}\"...");

                var registryKey = Registry.CurrentUser;
                var subKeyName = CustomServiceInstaller.RegistrySubkey + className;
                var subKey = registryKey.CreateSubKey(subKeyName);

                if (subKey == null)
                    throw new Exception($"Failed to load or create the registry sub key \"{subKeyName}\".");

                foreach (var property in typeof (T).GetProperties())
                {
                    var value = property.GetValue(obj, null);
                    Tracing.Current.Trace(TraceEventType.Verbose, $"Saving property \"{property.Name}\" with value \"{value}\"...");
                    subKey.SetValue(property.Name, value);
                }

                subKey.Close();
                Tracing.Current.Trace(TraceEventType.Verbose, "Saved registry settings.");
            }
        }

        public static T Load<T>() where T : class
        {
            using (Tracing.Current.NewScope())
            {
                var className = typeof (T).Name;
                Tracing.Current.Trace(TraceEventType.Verbose, $"Loading registry settings for \"{className}\"...");

                var registryKey = Registry.CurrentUser;
                var subKeyName = CustomServiceInstaller.RegistrySubkey + className;
                var subKey = registryKey.OpenSubKey(subKeyName);

                if (subKey == null)
                {
                    Tracing.Current.Trace(TraceEventType.Warning, $"No registry settings found for \"{subKeyName}\".");
                    return null;
                }

                var registrySettings = Activator.CreateInstance<T>();

                try
                {
                    foreach (var property in typeof (T).GetProperties())
                    {
                        Tracing.Current.Trace(TraceEventType.Verbose, $"Loading property \"{property.Name}\"...");
                        property.SetValue(registrySettings, subKey.GetValue(property.Name));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to load registry settings. {ex.Message}", ex);
                }
                finally
                {
                    subKey.Close();
                }

                Tracing.Current.Trace(TraceEventType.Verbose, "Loaded registry settings.");
                return registrySettings;
            }
        }
    }
}