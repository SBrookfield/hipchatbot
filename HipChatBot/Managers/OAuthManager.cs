﻿#region Copyright

// Filename: OAuthManager.cs
// Date Created: 11/05/2016 19:38
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using HipChatBot.Enums;
using HipChatBot.ExtensionMethods;
using HipChatBot.Instrumentation;
using HipChatBot.Models;
using HipChatBot.Models.Registry;
using Newtonsoft.Json;

namespace HipChatBot.Managers
{
    public static class OAuthManager
    {
        public static string AccessToken => GetAccessToken();

        private static OAuthSetting _oAuthSetting;
        private static DateTime? TokenExpiryDate { set; get; }
        private static string _accessToken;

        private static string GetAccessToken()
        {
            using (Tracing.Current.NewScope())
            {
                if (_oAuthSetting == null)
                    _oAuthSetting = RegistryManager.Load<OAuthSetting>();

                if (TokenExpiryDate > DateTime.Now || _oAuthSetting == null)
                {
                    Tracing.Current.Trace(TraceEventType.Information, "No authorization token, or authorization token expired.");
                    return _accessToken;
                }

                Tracing.Current.Trace(TraceEventType.Information, "Generating authorization token request...");
                using (var webClient = new WebClient())
                {
                    var authorizationToken = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_oAuthSetting.OauthId}:{_oAuthSetting.OauthSecret}"));

                    webClient.Headers.Add(HttpRequestHeader.Authorization, $"Basic {authorizationToken}");
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");

                    var nameValueCollection = new NameValueCollection {{"grant_type", "client_credentials"}, {"scope", Enum.GetValues(typeof (ScopeTypes)).Cast<ScopeTypes>().Select(e => e.Description()).Aggregate((p, n) => $"{p} {n}")}};

                    var hipChatUri = new Uri(ConfigurationManager.AppSettings["hipChatApiAddress"]);
                    var oAuthUri = new Uri(hipChatUri, "/v2/oauth/token");

                    Tracing.Current.Trace(TraceEventType.Verbose, "Requesting authorization token from HipChat...");
                    var resultString = Encoding.Default.GetString(webClient.UploadValues(oAuthUri, nameValueCollection));
                    var oAuthReply = JsonConvert.DeserializeObject<OAuthResponseModel>(resultString);

                    TokenExpiryDate = DateTime.Now.AddSeconds(oAuthReply.ExpiresIn - 5);

                    Tracing.Current.Trace(TraceEventType.Information, $"Received new authorization token.\nToken: {oAuthReply.AccessToken}.\nExpiry: {TokenExpiryDate}.");
                    return _accessToken = oAuthReply.AccessToken;
                }
            }
        }
    }
}