﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace HipChatBot
{
    [RunInstaller(true)]
    public class ServiceInstaller : Installer
    {
        public ServiceInstaller()
        {
            var serviceProcessInstaller = new ServiceProcessInstaller
            {
                Account = ServiceAccount.User,
                Username = null,
                Password = null
            };

            var serviceInstaller = new CustomServiceInstaller
            {
                // TODO: Move to registry.
                DisplayName = "HipChat Bot",
                ServiceName = "HipChatBot",
                Description = "The service used for running the HipChat Bot.",
                StartType = ServiceStartMode.Automatic
            };

            Installers.AddRange(new Installer[] {serviceProcessInstaller, serviceInstaller});
        }
    }
}