#region Copyright

// Filename: MessageBusManager.cs
// Date Created: 18/05/2016 00:15
// Copyright � 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System;
using System.Collections.Generic;
using HipChatBot.Shared.Interfaces;

namespace HipChatBot
{
    public class MessageBus : IMessageBus
    {
        public static MessageBus Current => _current ?? (_current = new MessageBus());

        private static readonly Dictionary<Type, List<Action<object>>> _actionsByType;
        private static MessageBus _current;

        static MessageBus()
        {
            _actionsByType = new Dictionary<Type, List<Action<object>>>();
        }

        public void Subscribe<T>(Action<T> action) where T : class
        {
            var type = typeof (T);
            var genericAction = new Action<object>(o => action((T) o));

            if (_actionsByType.ContainsKey(type))
                _actionsByType[type].Add(genericAction);
            else
                _actionsByType.Add(type, new List<Action<object>> {genericAction});
        }

        public void Send<T>(T message) where T : class
        {
            var type = typeof (T);

            if (!_actionsByType.ContainsKey(type))
                return;

            var actions = _actionsByType[type];

            foreach (var action in actions)
                action.Invoke(message);
        }
    }
}