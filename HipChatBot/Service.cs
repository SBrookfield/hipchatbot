﻿using System.ServiceProcess;

namespace HipChatBot
{
    public class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Program.Start();
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            Program.Stop();
            base.OnStop();
        }

        private void InitializeComponent()
        {
            ServiceName = "HipChatBot";
        }
    }
}