﻿using System;
using System.Diagnostics;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using Microsoft.Owin.Hosting;
using HipChatBot.ExtensionMethods;
using HipChatBot.Instrumentation;
using static System.Configuration.ConfigurationManager;

namespace HipChatBot
{
    internal static class Program
    {
        private static IDisposable _webApp;
        private static Plugins _plugins;

        private static void Main()
        {
            using (Tracing.Current.NewScope())
            {
                if (!Environment.UserInteractive)
                {
                    ServiceBase.Run(new Service());
                    return;
                }

                var serviceBase = typeof (ServiceBase).GetMethod("OnStart", BindingFlags.Instance | BindingFlags.NonPublic);
                serviceBase.Invoke(new Service(), new object[] {new string[] {}});
                Console.ReadKey(true);
            }
        }

        internal static void Start()
        {
            using (Tracing.Current.NewScope())
            {
                try
                {
                    if (bool.Parse(AppSettings["allowInsecureCertificates"]))
                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    StartWebApiServer();
                    StartHipChatBot();
                }
                catch (Exception ex)
                {
                    Tracing.Current.Trace(TraceEventType.Error, $"An error occured whilst starting HipChatBot. {ex.Message}");
                }
            }
        }

        internal static void Stop()
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Information, "Stopping WebApp...");
                _webApp?.Dispose();
                Tracing.Current.Trace(TraceEventType.Information, "Stopping Plugins...");
                _plugins?.Stop();
                Tracing.Current.Trace(TraceEventType.Information, "Application stopped.");
            }
        }

        private static void StartWebApiServer()
        {
            var portBinding = AppSettings["portBinding"];

            if (portBinding.IsNullOrEmpty())
                throw new Exception("Base address is not set in the config.");

            var baseAddress = $"http://+:{portBinding}/";
            Tracing.Current.Trace(TraceEventType.Information, $"Starting WebAPI server on \"{baseAddress}\"...");
            _webApp = WebApp.Start<WebApiConfig>(baseAddress);
            Tracing.Current.Trace(TraceEventType.Information, "WebAPI server started.");
        }

        private static void StartHipChatBot()
        {
            Tracing.Current.Trace(TraceEventType.Information, "Starting HipChatBot...");
            _plugins = new Plugins();
            _plugins.Start();
            Tracing.Current.Trace(TraceEventType.Information, "HipChatBot started.");
        }
    }
}