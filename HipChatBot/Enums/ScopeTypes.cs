﻿#region Copyright

// Filename: ScopeTypes.cs
// Date Created: 10/05/2016 01:42
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System.ComponentModel;

namespace HipChatBot.Enums
{
    public enum ScopeTypes
    {
        [Description("admin_group")] AdminGroup,
        [Description("admin_room")] AdminRoom,
        [Description("import_data")] ImportData,
        [Description("manage_rooms")] ManageRooms,
        [Description("send_message")] SendMessage,
        [Description("send_notification")] SendNotification,
        [Description("view_group")] ViewGroup,
        [Description("view_messages")] ViewMessages,
        [Description("view_room")] ViewRoom
    }
}