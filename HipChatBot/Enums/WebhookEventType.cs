﻿#region Copyright

// Filename: WebhookEventType.cs
// Date Created: 09/05/2016 22:21
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System.ComponentModel;

namespace HipChatBot.Enums
{
    public enum WebhookEventType
    {
        [Description("room_archived")] RoomArchived,
        [Description("room_created")] RoomCreated,
        [Description("room_deleted")] RoomDeleted,
        [Description("room_enter")] RoomEnter,
        [Description("room_exit")] RoomExit,
        [Description("room_file_upload")] RoomFileUpload,
        [Description("room_message")] RoomMessage,
        [Description("room_notification")] RoomNotification,
        [Description("room_topic_change")] RoomTopicChange,
        [Description("room_unarchived")] RoomUnarchived
    }
}