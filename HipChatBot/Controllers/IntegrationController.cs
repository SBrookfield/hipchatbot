﻿#region Copyright
// Filename: IntegrationController.cs
// Date Created: 10/05/2016 00:05
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Http;
using HipChatBot.Enums;
using HipChatBot.ExtensionMethods;
using HipChatBot.Instrumentation;
using HipChatBot.Managers;
using HipChatBot.Models.Capabilities;
using HipChatBot.Models.Registry;
using HipChatBot.Models.Webhook;
using static System.Configuration.ConfigurationManager;

namespace HipChatBot.Controllers
{
    public class IntegrationController : BaseApiController
    {
        [HttpGet]
        public CapabilitiesDescriptorModel Capabilities()
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Verbose, "Returning capabilities...");
                return new CapabilitiesDescriptorModel
                {
                    Name = AppSettings["integrationName"],
                    Description = AppSettings["integrationDescription"],
                    Key = AppSettings["integrationKey"],
                    Vendor = new VendorModel
                    {
                        Name = AppSettings["integrationAuthor"],
                        Url = AppSettings["integrationAuthorUrl"]
                    },
                    Capabilities = new CapabilitiesModel
                    {
                        HipchatApiConsumer = new HipchatApiConsumerModel
                        {
                            FromName = AppSettings["integrationBotName"],
                            Scopes = Enum.GetValues(typeof(ScopeTypes)).Cast<ScopeTypes>().Select(s => s.Description()).ToArray()
                        },
                        Installable = new InstallableModel
                        {
                            CallbackUrl = GetUrl(nameof(Installed))
                        },
                        Webhook = new []
                        {
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomArchived.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomArchived), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomCreated.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomCreated), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomDeleted.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomDeleted), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomEnter.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomEnter), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomExit.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomExit), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomFileUpload.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomFileUpload), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomMessage.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomMessage), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomNotification.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomNotification), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomTopicChange.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomTopicChange), nameof(WebhookController))
                            },
                            new WebhookModel
                            {
                                Event = WebhookEventType.RoomUnarchived.Description(),
                                Url = GetUrl(nameof(WebhookController.RoomUnarchived), nameof(WebhookController))
                            }
                        }
                    },
                    Links = new LinkModel
                    {
                        Homepage = GetUrl(nameof(Home)),
                        Self = CurrentUrl
                    }
                };
            }
        }
        

        [HttpPost]
        public void Installed(OAuthSetting oAuthSetting)
        {
            using (Tracing.Current.NewScope())
            {
                Tracing.Current.Trace(TraceEventType.Verbose, "Received installation information.");

                var oAuth = new OAuthSetting {OauthId = oAuthSetting.OauthId, OauthSecret = oAuthSetting.OauthSecret};

                try
                {
                    RegistryManager.Save(oAuth);
                }
                catch (Exception ex)
                {
                    Tracing.Current.Trace(TraceEventType.Error, $"An error occured whilst saving the registry settings. {ex.Message}");
                    StatusCode(HttpStatusCode.InternalServerError);
                }
            }
        }

        [HttpGet]
        public string Home()
        {
            return "Test OK!";
        }
    }
}