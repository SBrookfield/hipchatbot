﻿#region Copyright

// Filename: WebhookController.cs
// Date Created: 07/05/2016 00:35
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System.Diagnostics;
using System.Web.Http;
using HipChatBot.Instrumentation;
using HipChatBot.Models.Webhook;
using HipChatBot.Shared.Messages;

namespace HipChatBot.Controllers
{
    public class WebhookController : BaseApiController
    {
        [HttpPost]
        public void RoomArchived(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var archiverId = webhookEventModel.Item.Sender.Id;
                var archiverName = webhookEventModel.Item.Sender.Name;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{archiverName}\" has archived the room \"{roomName}\".");
                MessageBus.Current.Send(new RoomArchivedMessage(roomId, archiverId));
            }
        }

        [HttpPost]
        public void RoomUnarchived(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var unarchiverId = webhookEventModel.Item.Sender.Id;
                var unarchiverName = webhookEventModel.Item.Sender.Name;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{unarchiverName}\" has unarchived the room \"{roomName}\".");
                MessageBus.Current.Send(new RoomUnarchivedMessage(roomId, unarchiverId));
            }
        }

        [HttpPost]
        public void RoomCreated(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var creatorId = webhookEventModel.Item.Sender.Id;
                var creatorName = webhookEventModel.Item.Sender.Name;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{creatorName}\" has created the room \"{roomName}\".");
                MessageBus.Current.Send(new RoomCreatedMessage(roomId, creatorId));
            }
        }

        [HttpPost]
        public void RoomDeleted(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var deleterId = webhookEventModel.Item.Sender.Id;
                var deleterName = webhookEventModel.Item.Sender.Name;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{deleterName}\" has deleted the room \"{roomName}\".");
                MessageBus.Current.Send(new RoomDeletedMessage(roomId, deleterId));
            }
        }

        [HttpPost]
        public void RoomEnter(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var userId = webhookEventModel.Item.Sender.Id;
                var userName = webhookEventModel.Item.Sender.Name;
                var roomName = webhookEventModel.Item.Room.Name;
                var roomId = webhookEventModel.Item.Room.Id;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{userName}\" has joined \"{roomName}\".");
                MessageBus.Current.Send(new RoomEnteredMessage(roomId, userId));
            }
        }

        [HttpPost]
        public void RoomExit(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var userId = webhookEventModel.Item.Sender.Id;
                var userName = webhookEventModel.Item.Sender.Name;
                var roomName = webhookEventModel.Item.Room.Name;
                var roomId = webhookEventModel.Item.Room.Id;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{userName}\" has left \"{roomName}\".");
                MessageBus.Current.Send(new RoomExitedMessage(roomId, userId));
            }
        }

        [HttpPost]
        public void RoomMessage(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var senderName = webhookEventModel.Item.Message.User.Name;
                var senderId = webhookEventModel.Item.Message.User.Id;
                var message = webhookEventModel.Item.Message.Text;

                Tracing.Current.Trace(TraceEventType.Verbose, $"Message received from \"{senderName}\" in \"{roomName}\".");
                MessageBus.Current.Send(new RoomMessagedMessage(roomId, senderId, message));
            }
        }

        [HttpPost]
        public void RoomNotification(WebhookNotificationEventModel webhookNotificationEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookNotificationEventModel.Item.Room.Id;
                var roomName = webhookNotificationEventModel.Item.Room.Name;
                var integrationName = webhookNotificationEventModel.Item.Message.From;
                var message = webhookNotificationEventModel.Item.Message.Text;

                Tracing.Current.Trace(TraceEventType.Verbose, $"Notification received from \"{integrationName}\" in \"{roomName}\".");
                MessageBus.Current.Send(new RoomNotificationMessage(roomId, integrationName, message));
            }
        }

        [HttpPost]
        public void RoomFileUpload(WebhookEventModel webhookEventModel)
        {
            var roomId = webhookEventModel.Item.Room.Id;
            var roomName = webhookEventModel.Item.Room.Name;
            var senderName = webhookEventModel.Item.Sender.Name;
            var senderId = webhookEventModel.Item.Sender.Id;
            var fileName = webhookEventModel.Item.File.Name;
            var fileSize = webhookEventModel.Item.File.Size;
            var fileUrl = webhookEventModel.Item.File.Url;

            Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{senderName}\" has uploaded the file \"{fileName}\" ({(fileSize/1024f)/1024f} MB) to \"{roomName}\".");
            MessageBus.Current.Send(new RoomFileUploadedMessage(roomId, senderId, fileName, fileUrl, fileSize));
        }

        [HttpPost]
        public void RoomTopicChange(WebhookEventModel webhookEventModel)
        {
            using (Tracing.Current.NewScope())
            {
                var roomId = webhookEventModel.Item.Room.Id;
                var roomName = webhookEventModel.Item.Room.Name;
                var changerName = webhookEventModel.Item.Sender.Name;
                var changerId = webhookEventModel.Item.Sender.Id;
                var topic = webhookEventModel.Item.Topic;

                Tracing.Current.Trace(TraceEventType.Verbose, $"User \"{changerName}\" has change the topic for \"{roomName}\".");
                MessageBus.Current.Send(new RoomTopicChangedMessage(roomId, changerId, topic));
            }
        }
    }
}