﻿#region Copyright
// Filename: BaseApiController.cs
// Date Created: 10/05/2016 00:45
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Web.Http;

namespace HipChatBot.Controllers
{
    public class BaseApiController : ApiController
    {
        protected string CurrentUrl => Url.Link("Default", null);

        protected string GetUrl(string methodName, string controllerName = null)
        {
            if (controllerName == null)
                controllerName = GetType().Name;

            var controllerApiName = controllerName.Substring(0, controllerName.LastIndexOf("Controller", StringComparison.CurrentCultureIgnoreCase));

            return Url.Link("Default", new {controller = controllerApiName, action  = methodName}).ToLower();
        }
    }
}