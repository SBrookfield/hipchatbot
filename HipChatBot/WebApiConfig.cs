﻿using System.Net.Http.Formatting;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json.Serialization;
using Owin;

namespace HipChatBot
{
    public class WebApiConfig
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Routes.IgnoreRoute("Images", "images/{*pathInfo}");
            config.Routes.MapHttpRoute("Default", "{controller}/{action}/{id}", new {id = RouteParameter.Optional});
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            appBuilder.UseWebApi(config);
            appBuilder.UseFileServer(new FileServerOptions {
                EnableDirectoryBrowsing = true,
                RequestPath = new PathString("/images")
            });
        }
    }
}