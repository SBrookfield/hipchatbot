﻿#region Copyright

// Filename: CustomServiceInstaller.cs
// Date Created: 11/05/2016 13:03
// Copyright © 2016 Sebastian Brookfield
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details. <http://www.gnu.org/licenses/>.

#endregion

using System.Collections;

namespace HipChatBot
{
    // TODO: Move app config integration stuff into registry.
    public class CustomServiceInstaller: System.ServiceProcess.ServiceInstaller
    {
        public const string RegistrySubkey = @"SOFTWARE\SBrookfield\HipChatBot\";

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }
    }
}